var http = require('http'),
    fs = require('fs'),
    util = require('util'),
    redis = require('redis');

var redisClient = redis.createClient(process.env.CLOUDRON_REDIS_PORT, process.env.CLOUDRON_REDIS_HOST);
redisClient.auth(process.env.CLOUDRON_REDIS_PASSWORD);
redisClient.on("error", function (err) {
  console.log("Redis Client Error " + err);
});

var COUNTER_KEY = 'counter';

var server = http.createServer(function (request, response) {
  redisClient.get(COUNTER_KEY, function (err, reply) {
    var counter = (!err && reply) ? parseInt(reply, 10) : 0;
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.end(util.format("Hello World. %s visitors have visited this page\n", counter));
    redisClient.incr(COUNTER_KEY);
  });
});

server.listen(8000);

console.log("Server running at port 8000");
