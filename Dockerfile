FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

ADD server.js /app/code/server.js
ADD package.json /app/code/package.json

WORKDIR /app/code
RUN npm install --production

CMD [ "node", "/app/code/server.js" ]
